/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dao;

import br.ufscar.bdnc.dto.BaseDTO;
import java.util.List;

/**
 *
 * @author fabiomf
 * @param <T>
 */
public interface DAO<T extends BaseDTO> {

    public void inserir(T dto);

    public void update(T dto);

    public void delete(T dto);

    public List<T> obterDados();
    
}
