/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufscar.bdnc.dao;

import br.ufscar.bdnc.dto.Medico;
import java.util.List;

/**
 *
 * @author fabio.medeiros
 */
public interface MedicoDAO extends DAO<Medico>{
    
    public List<Medico> obterMedicosPorNome(String nome);
    
}
