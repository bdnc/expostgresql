/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dao;

import br.ufscar.bdnc.dto.Medico;
import br.ufscar.bdnc.dto.Paciente;
import java.util.List;

/**
 *
 * @author sandra
 */
public interface PacienteDAO extends DAO<Paciente>{ 
    
    public List<Paciente> obterPacientePorNome(String nome);
    
    public void atualizarConsulta(Paciente paciente);
    
    public List<Medico> buscarMedicosPorPaciente(Paciente paciente);
    
    public Integer obterQuantidadeConsultas(Integer cpf);
    
}
