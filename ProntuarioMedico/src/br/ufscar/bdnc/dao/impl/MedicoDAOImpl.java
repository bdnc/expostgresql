/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dao.impl;

import br.ufscar.bdnc.dao.MedicoDAO;
import br.ufscar.bdnc.dto.Endereco;
import br.ufscar.bdnc.dto.Medico;
import br.ufscar.bdnc.dto.Telefone;
import br.ufscar.bdnc.dto.TipoTelefoneEnum;
import br.ufscar.bdnc.util.Conexao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fabiomf
 */
public class MedicoDAOImpl implements MedicoDAO{

    @Override
    public List<Medico> obterMedicosPorNome(String nome) {
        StringBuilder sql = new StringBuilder();
        sql.append("select (pess).prenome, (pess).sobrenome, (pess).cpf, (medico).crm ")
                .append(" from tb_medico ")
                .append(" where (pess).prenome ilike ? ");
        
        PreparedStatement prest = null;
        ResultSet result = null;
        
        List<Medico> listaPessoas = new ArrayList<>();
        Medico medicoObtido = null;
        
        try {
            prest = Conexao.getInstance().getConnection().prepareStatement(sql.toString());
            
            prest.setString(1, nome + "%");
            
            result = prest.executeQuery();
            
            while(result.next()) {
                
                medicoObtido = new Medico();
                medicoObtido.setPrenome(result.getString("prenome"));
                medicoObtido.setSobrenome(result.getString("sobrenome"));
                medicoObtido.setCpf(result.getInt("cpf"));
                medicoObtido.setCrm(result.getInt("crm"));
                
                listaPessoas.add(medicoObtido);
                //System.out.println(medicoObtido.getCrm());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
                prest.close();

            } catch (SQLException ex) {
                Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return listaPessoas;
    }

    @Override
    public void inserir(Medico medico) {
        
        PreparedStatement prest = null;
        ResultSet result = null;
                
        try {
            prest = Conexao.getInstance().getConnection().prepareStatement(montarSql(medico));
           
            prest.setInt(1, medico.getCrm());
            prest.setInt(2, medico.getCpf());
            prest.setString(3, medico.getPrenome());
            prest.setString(4, medico.getSobrenome());
            prest.setString(5, medico.getEndereco().getRua());
            prest.setInt(6, medico.getEndereco().getNumero());
            prest.setString(7, medico.getEndereco().getBairro());
            prest.setInt(8, medico.getEndereco().getCep());
            prest.setString(9, medico.getEndereco().getCidade());
            prest.setString(10, medico.getEndereco().getUf());
            
            //TODO: email
            prest.setString(11, null);
            prest.setString(12, null);
            
            if (medico.getListaTelefones() != null) {
                
                int indexInsertTelefone = 12;
                for (Telefone telefone : medico.getListaTelefones()) {
                   
                    prest.setInt(++indexInsertTelefone, telefone.getDdd());
                    prest.setInt(++indexInsertTelefone, telefone.getPrefixo());
                    prest.setInt(++indexInsertTelefone, telefone.getNumero());
                    prest.setInt(++indexInsertTelefone, telefone.getRamal());
                    prest.setString(++indexInsertTelefone, telefone.getTipoTelefoneEnum().getTipo());
                    
                }
            }
            
            prest.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Medico dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Medico dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Medico> obterDados() {       
       
            String sql = "select (medico).crm as crm, (pess).cpf, (pess).prenome, (pess).sobrenome "
                + ", (pess).endereco.rua as rua "
                + ", (pess).endereco.numero as numero "
                + ", (pess).endereco.bairro as bairro "
                + ", (pess).endereco.cep as cep "
                + ", (pess).endereco.uf as uf "
                + ", (pess).email1 as email1 "
                + ", (pess).telefone[0].ddd as ddd "
                + ", (pess).telefone[0].prefixo as prefixo "
                + ", (pess).telefone[0].numero as numero "
                + ", (pess).telefone[0].ramal as ramal "
                + ", (pess).telefone[0].tipo as tipo "
                + "from tb_medico";

        PreparedStatement prest = null;
        ResultSet result = null;

        List<Medico> listaPessoas = new ArrayList<>();
        Medico medicoObtido = null;
        Endereco enderecoObtido = new Endereco();
        List<String> listaEmailsObtida = new ArrayList<>();
        List<Telefone> listaTelefoneObtida = new ArrayList<>();
        Telefone telefoneObtido = null;
     
        try {

            prest = Conexao.getInstance().getConnection().prepareStatement(sql);

            result = prest.executeQuery();

            while (result.next()) {

                medicoObtido = new Medico();
                
                medicoObtido.setCrm(result.getInt("crm"));
                
                medicoObtido.setCpf(result.getInt("cpf"));
                medicoObtido.setPrenome(result.getString("prenome"));
                medicoObtido.setSobrenome(result.getString("sobrenome"));

                enderecoObtido.setRua(result.getString("rua"));
                enderecoObtido.setNumero(result.getInt("numero"));
                enderecoObtido.setBairro(result.getString("bairro"));
                enderecoObtido.setCep(result.getInt("cep"));
                enderecoObtido.setUf(result.getString("uf"));
                medicoObtido.setEndereco(enderecoObtido);

                listaEmailsObtida.add(result.getString("email1"));
                medicoObtido.setEmails(listaEmailsObtida);

                telefoneObtido = new Telefone();
                telefoneObtido.setDdd(result.getInt("ddd"));
                telefoneObtido.setPrefixo(result.getInt("prefixo"));
                telefoneObtido.setNumero(result.getInt("numero"));
                telefoneObtido.setRamal(result.getInt("ramal"));
                telefoneObtido.setTipoTelefoneEnum(TipoTelefoneEnum.COMERCIAL);
                listaTelefoneObtida.add(telefoneObtido);
                medicoObtido.setListaTelefones(listaTelefoneObtida);

                //TODO: inserir todos os campos.
                listaPessoas.add(medicoObtido);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
                prest.close();

            } catch (SQLException ex) {
                Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return listaPessoas;
    }
    
    private String montarSql(Medico medico) {
        
        StringBuilder sql = new StringBuilder();
        StringBuilder parametros = new StringBuilder();
        
        sql.append("insert into  tb_medico ( ")
            .append(" medico.crm, ")
            .append(" pess.cpf, ") 
            .append(" pess.prenome, ")
            .append(" pess.sobrenome, ")
            .append(" pess.endereco.rua, ")
            .append(" pess.endereco.numero, ")
            .append(" pess.endereco.bairro, ")
            .append(" pess.endereco.cep, ")
            .append(" pess.endereco.cidade, ")
            .append(" pess.endereco.uf, ")
            .append(" pess.email1, ")
            .append(" pess.email2 ");
        
        parametros.append(" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ");
        
        if (medico != null && medico.getListaTelefones() != null) {
            
            int index = 0;
            
            for (Telefone telefone : medico.getListaTelefones()) {
                
                sql.append(", pess.telefone[").append(index).append("].ddd, ")
                    .append(" pess.telefone[").append(index).append("].prefixo, ")
                    .append(" pess.telefone[").append(index).append("].numero, ")
                    .append(" pess.telefone[").append(index).append("].ramal, ")
                    .append(" pess.telefone[").append(index).append("].tipo ");
                
                parametros.append((" , ?, ?, ?, ?, ? "));
                index++;
            }
        }
        
        sql.append(")");
        
        parametros.append(")");
        
        sql.append(parametros.toString());
        
        return sql.toString();
    }
}
