/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dao.impl;
import br.ufscar.bdnc.dao.ExameDAO;
import br.ufscar.bdnc.dto.Exame;
import br.ufscar.bdnc.util.Conexao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Raquel
 */
public class ExameDAOImpl implements ExameDAO{
    private PreparedStatement stm;
    private Conexao con;
    ResultSet result;
    Exame exame;
   
    public ExameDAOImpl(){
    };
    
    @Override
    public void inserir(Exame exame){
        try {
            con = Conexao.getInstance();
            stm = con.getConnection().prepareStatement("insert into tb_exame(exame.descricao,exame.nome,exame.indicereferencia) values (?,?,?)");
        
            stm.setString(1, exame.getDescricao());
            
            stm.setString(2, exame.getNome());
            
            stm.setString(3, exame.getIndiceReferencia());
            stm.execute();
            con.closeConnection();
            
            JOptionPane.showMessageDialog(null, "Dados Gravados");

            
        } catch (SQLException ex) {
            System.out.println("Erro de SQL"+ ex);        
        }
    }

    @Override
    public void update(Exame dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Exame dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Exame> obterDados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void listarExames() {
        
        try{
            con = Conexao.getInstance();
            stm = con.getConnection().prepareStatement("select * from tb_exame");
            con = Conexao.getInstance();
            result = stm.executeQuery();
             
             
        }
        catch (SQLException error) {
            JOptionPane.showMessageDialog(null,error); 
    }
}  

    @Override
    public List<Exame> obterExamePorNome(String nome) {
        
        String sql = "select (exame).descricao,(exame).nome,(exame).indicereferencia   from tb_exame where (exame).nome = ? ";
        
        PreparedStatement prest = null;
        ResultSet result = null;

        List<Exame> listaExames = new ArrayList<>();
        Exame exameObtido = null;

        try {
            prest = Conexao.getInstance().getConnection().prepareStatement(sql.toString());

            prest.setString(1, nome);

            result = prest.executeQuery();

            while (result.next()) {

                exameObtido = new Exame();
                exameObtido.setDescricao(result.getString("descricao"));
                exameObtido.setNome(result.getString("nome"));
                exameObtido.setIndiceReferencia(result.getString("indicereferencia"));

                //TODO: inserir todos os campos.
                listaExames.add(exameObtido);
         }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
                prest.close();

            } catch (SQLException ex) {
                Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return listaExames;
    }
}
