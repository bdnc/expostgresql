/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dao.impl;

import br.ufscar.bdnc.dao.PacienteDAO;
import br.ufscar.bdnc.dto.Consulta;
import br.ufscar.bdnc.dto.Endereco;
import br.ufscar.bdnc.dto.Medico;
import br.ufscar.bdnc.dto.Paciente;
import br.ufscar.bdnc.dto.Prontuario;
import br.ufscar.bdnc.dto.Telefone;
import br.ufscar.bdnc.dto.TipoTelefoneEnum;
import br.ufscar.bdnc.util.Conexao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sandra
 */
public class PacienteDAOImpl implements PacienteDAO {

    @Override
    public void inserir(Paciente paciente) {

        PreparedStatement prest = null;

        if (paciente.getEndereco().getNumero() == null) {
            paciente.getEndereco().setNumero(0);
        }

        if (paciente.getEndereco().getCep() == null) {
            paciente.getEndereco().setCep(0);
        }

        try {
            prest = Conexao.getInstance().getConnection().prepareStatement(montarSql(paciente));

            prest.setInt(1, paciente.getCpf());
            prest.setString(2, paciente.getPrenome());
            prest.setString(3, paciente.getSobrenome());
            prest.setString(4, paciente.getEndereco().getRua());
            prest.setInt(5, paciente.getEndereco().getNumero());
            prest.setString(6, paciente.getEndereco().getBairro());
            prest.setInt(7, paciente.getEndereco().getCep());
            prest.setString(8, paciente.getEndereco().getCidade());
            prest.setString(9, paciente.getEndereco().getUf());

            //TODO: email
            prest.setString(10, null);
            prest.setString(11, null);

            if (paciente.getListaTelefones() != null) {

                int indexInsertTelefone = 11;
                for (Telefone telefone : paciente.getListaTelefones()) {

                    prest.setInt(++indexInsertTelefone, telefone.getDdd());
                    prest.setInt(++indexInsertTelefone, telefone.getPrefixo());
                    prest.setInt(++indexInsertTelefone, telefone.getNumero());
                    prest.setInt(++indexInsertTelefone, telefone.getRamal());
                    prest.setString(++indexInsertTelefone, telefone.getTipoTelefoneEnum().getTipo());

                }
            }

            prest.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void update(Paciente dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Paciente dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Paciente> obterPacientePorNome(String nome) {

        StringBuilder sql = new StringBuilder();
        sql.append("select (pess).prenome, (pess).sobrenome, (pess).cpf ")
                .append(" from tb_paciente ")
                .append(" where (pess).prenome ilike ? ");

        PreparedStatement prest = null;
        ResultSet result = null;

        List<Paciente> listaPessoas = new ArrayList<>();
        Paciente pacienteObtido = null;

        try {
            prest = Conexao.getInstance().getConnection().prepareStatement(sql.toString());

            prest.setString(1, nome + "%");

            result = prest.executeQuery();

            while (result.next()) {

                pacienteObtido = new Paciente();
                pacienteObtido.setPrenome(result.getString("prenome"));
                pacienteObtido.setSobrenome(result.getString("sobrenome"));
                pacienteObtido.setCpf(result.getInt("cpf"));

                //TODO: inserir todos os campos.
                listaPessoas.add(pacienteObtido);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
                prest.close();

            } catch (SQLException ex) {
                Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return listaPessoas;
    }

    @Override
    public List<Paciente> obterDados() {
        String sql = "select (pess).cpf, (pess).prenome, (pess).sobrenome "
                + ", (pess).endereco.rua as rua "
                + ", (pess).endereco.numero as numero "
                + ", (pess).endereco.bairro as bairro "
                + ", (pess).endereco.cep as cep "
                + ", (pess).endereco.uf as uf "
                + ", (pess).email1 as email1 "
                + ", (pess).telefone[0].ddd as ddd "
                + ", (pess).telefone[0].prefixo as prefixo "
                + ", (pess).telefone[0].numero as numero "
                + ", (pess).telefone[0].ramal as ramal "
                + ", (pess).telefone[0].tipo as tipo "
                + "from tb_paciente";

        PreparedStatement prest = null;
        ResultSet result = null;

        List<Paciente> listaPessoas = new ArrayList<>();
        Paciente pacienteObtido = null;
        Endereco enderecoObtido = new Endereco();
        List<String> listaEmailsObtida = new ArrayList<>();
        List<Telefone> listaTelefoneObtida = new ArrayList<>();
        Telefone telefoneObtido = null;
        Prontuario prontuarioObtido = new Prontuario();

        try {

            prest = Conexao.getInstance().getConnection().prepareStatement(sql);

            result = prest.executeQuery();

            while (result.next()) {

                pacienteObtido = new Paciente();
                pacienteObtido.setCpf(result.getInt("cpf"));
                pacienteObtido.setPrenome(result.getString("prenome"));
                pacienteObtido.setSobrenome(result.getString("sobrenome"));

                enderecoObtido.setRua(result.getString("rua"));
                enderecoObtido.setNumero(result.getInt("numero"));
                enderecoObtido.setBairro(result.getString("bairro"));
                enderecoObtido.setCep(result.getInt("cep"));
                enderecoObtido.setUf(result.getString("uf"));
                pacienteObtido.setEndereco(enderecoObtido);

                listaEmailsObtida.add(result.getString("email1"));
                pacienteObtido.setEmails(listaEmailsObtida);

                telefoneObtido = new Telefone();
                telefoneObtido.setDdd(result.getInt("ddd"));
                telefoneObtido.setPrefixo(result.getInt("prefixo"));
                telefoneObtido.setNumero(result.getInt("numero"));
                telefoneObtido.setRamal(result.getInt("ramal"));
                telefoneObtido.setTipoTelefoneEnum(TipoTelefoneEnum.COMERCIAL);
                listaTelefoneObtida.add(telefoneObtido);
                pacienteObtido.setListaTelefones(listaTelefoneObtida);

                //TODO: inserir todos os campos.
                listaPessoas.add(pacienteObtido);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
                prest.close();

            } catch (SQLException ex) {
                Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return listaPessoas;
    }

    private String montarSql(Paciente paciente) {

        StringBuilder sql = new StringBuilder();
        StringBuilder parametros = new StringBuilder();

        sql.append("insert into  tb_paciente ( ")
                .append(" pess.cpf, ")
                .append(" pess.prenome, ")
                .append(" pess.sobrenome, ")
                .append(" pess.endereco.rua, ")
                .append(" pess.endereco.numero, ")
                .append(" pess.endereco.bairro, ")
                .append(" pess.endereco.cep, ")
                .append(" pess.endereco.cidade, ")
                .append(" pess.endereco.uf, ")
                .append(" pess.email1, ")
                .append(" pess.email2 ");

        parametros.append(" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ");

        if (paciente != null && paciente.getListaTelefones() != null) {

            int index = 0;

            for (Telefone telefone : paciente.getListaTelefones()) {

                sql.append(", pess.telefone[").append(index).append("].ddd, ")
                        .append(" pess.telefone[").append(index).append("].prefixo, ")
                        .append(" pess.telefone[").append(index).append("].numero, ")
                        .append(" pess.telefone[").append(index).append("].ramal, ")
                        .append(" pess.telefone[").append(index).append("].tipo ");

                parametros.append((" , ?, ?, ?, ?, ? "));
                index++;
            }
        }

        sql.append(")");

        parametros.append(")");

        sql.append(parametros.toString());

        return sql.toString();
    }

    @Override
    public void atualizarConsulta(Paciente paciente) {
        PreparedStatement prest = null;

        try {
            prest = Conexao.getInstance().getConnection().
                    prepareStatement(
                            " update tb_paciente set paciente.consulta[0].crm = ?, "
                            + " paciente.consulta[0].datahora = ? "
                            + " where (pess).cpf = ?");

            prest.setInt(1, paciente.getListaConsultas().get(0).getCrm());

            prest.setTime(2, new java.sql.Time(paciente.getListaConsultas().get(0).getData().getTime()));

            prest.setInt(3, paciente.getCpf());

            prest.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Medico> buscarMedicosPorPaciente(Paciente paciente) {
        
        List<Medico> listaMedicos = new ArrayList<>();
        
        Integer quantidadeConsultas = this.obterQuantidadeConsultas(paciente.getCpf());
        
        for (int posicaoConsulta = 0; posicaoConsulta <= quantidadeConsultas; posicaoConsulta++) {
            listaMedicos.addAll(this.obterConsultasPorMedico(paciente.getCpf(), posicaoConsulta));
            System.out.println("nom " + listaMedicos.get(0).getPrenome());
        }

        
        return listaMedicos;
    }

    private List<Medico> obterConsultasPorMedico(Integer cpf, Integer indiceConsulta) {
        
        StringBuilder sql = new StringBuilder();
        sql.append("select (m.pess).prenome ")
                .append(" from tb_medico m, tb_paciente p ")
                .append(" where (p.paciente).consulta[?].crm = (m.medico).crm ")
                .append(" and (p.pess).cpf = ?");
        
        PreparedStatement prest = null;
        ResultSet result = null;
        
        List<Medico> listaMedicos = new ArrayList<>();
        Medico medicoObtido = null;
        System.out.println("Posicao consulta " + indiceConsulta);
        try {
            
            prest = Conexao.getInstance().getConnection().prepareStatement(sql.toString());
            
            prest.setInt(1, indiceConsulta);
            prest.setInt(2, cpf);
            
            result = prest.executeQuery();
            
            while(result.next()) {
                medicoObtido = new Medico();
                medicoObtido.setPrenome(result.getString("prenome"));
                listaMedicos.add(medicoObtido);
            }
            
        } catch (SQLException e ) {
            e.printStackTrace();
        }
        
        return listaMedicos;
    }
    
    @Override
    public Integer obterQuantidadeConsultas(Integer cpf) {

        StringBuilder sql = new StringBuilder();
        sql.append(" select (paciente).consulta ")
                .append(" from tb_paciente ")
                .append("where (pess).cpf = ?");
        
        PreparedStatement prest = null;
        ResultSet result = null;
        String consultas = null;
        
        try {
            prest = Conexao.getInstance().getConnection().prepareStatement(sql.toString());
            
            prest.setInt(1, cpf);
            
            result = prest.executeQuery();
            
            if (result.next()) {
                consultas = result.getString("consulta");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        String[] valor = consultas.split("=");
        
        String valorTexto = valor[0];
        
        valorTexto = valorTexto.substring(3);
        valorTexto = valorTexto.substring(0, valorTexto.length() - 1);
            
        return Integer.valueOf(valorTexto);
        
    }
    
    

}
