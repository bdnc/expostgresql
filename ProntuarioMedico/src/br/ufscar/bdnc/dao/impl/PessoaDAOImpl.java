/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dao.impl;

import br.ufscar.bdnc.dao.PessoaDAO;
import br.ufscar.bdnc.dto.Medico;
import br.ufscar.bdnc.dto.Pessoa;
import br.ufscar.bdnc.util.Conexao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fabiomf
 */
public class PessoaDAOImpl implements PessoaDAO{

    @Override
    public Pessoa consultarPessoaPorCpf(Integer cpf) {
        String sql = "select (pess).cpf from tb_pessoa where (pess).cpf = ? ";
        
        PreparedStatement prest = null;
        ResultSet result = null;
        
        Pessoa pessoaObtida = null;
        
        try {
            prest = Conexao.getInstance().getConnection().prepareStatement(sql);
            
            prest.setInt(1, cpf);
            
            result = prest.executeQuery();
            
            if(result.next()) {
                
                pessoaObtida = new Medico();
                pessoaObtida.setCpf(result.getInt("cpf"));
               
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
                prest.close();

            } catch (SQLException ex) {
                Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return pessoaObtida;
    }
    
    @Override
    public void inserir(Pessoa pessoa) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Pessoa dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Pessoa dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Pessoa> obterDados() {
        
        String sql = "select (pess).endereco.rua as rua from tb_pessoa";
        
        PreparedStatement prest = null;
        ResultSet result = null;
        
        List<Pessoa> listaPessoas = new ArrayList<>();
        Pessoa pessoaObtida = null;
        
        try {
            prest = Conexao.getInstance().getConnection().prepareStatement(sql);
            
            result = prest.executeQuery();
            
            while(result.next()) {
                
                pessoaObtida = new Medico();
                //pessoaObtida.setPrenome(result.getString("prenome"));
                System.out.println(result.getString("rua"));
                //TODO: inserir todos os campos.
                listaPessoas.add(pessoaObtida);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
                prest.close();

            } catch (SQLException ex) {
                Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return listaPessoas;
    }
}
