/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dao;

import br.ufscar.bdnc.dto.Pessoa;

/**
 *
 * @author fabiomf
 */
public interface PessoaDAO extends DAO<Pessoa>{
    
    public Pessoa consultarPessoaPorCpf(Integer cpf);
}
