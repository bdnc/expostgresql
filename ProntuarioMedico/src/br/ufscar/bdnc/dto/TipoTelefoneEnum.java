/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dto;

/**
 *
 * @author fabiomf
 */
public enum TipoTelefoneEnum {
    
    COMERCIAL("Comercial"),
    RESIDENCIAL("Residencial"),
    CELULAR("Celular");
    
    TipoTelefoneEnum(String tipo) {
        this.tipo = tipo;
    }
    
    private String tipo;

    public String getTipo() {
        return tipo;
    }
    
    
}
