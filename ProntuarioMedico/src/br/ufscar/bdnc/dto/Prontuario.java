/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dto;

import java.util.Date;
import java.util.List;

/**
 *
 * @author fabiomf
 */
public class Prontuario implements BaseDTO {
    
    private Date data;
    
    private List<Anamnese> listaAnamneses;

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public List<Anamnese> getListaAnamneses() {
        return listaAnamneses;
    }

    public void setListaAnamneses(List<Anamnese> listaAnamneses) {
        this.listaAnamneses = listaAnamneses;
    }
    
    
}
