/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dto;

import java.util.Objects;

/**
 *
 * @author fabiomf
 */
public class Exame implements BaseDTO {
    
    private Integer codigoExame;
    
    private String descricao;
    
    private String nome;
    
    private String indiceReferencia;

    public Integer getCodigoExame() {
        return codigoExame;
    }

    public void setCodigoExame(Integer codigoExame) {
        this.codigoExame = codigoExame;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIndiceReferencia() {
        return indiceReferencia;
    }

    public void setIndiceReferencia(String indiceReferencia) {
        this.indiceReferencia = indiceReferencia;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.codigoExame);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Exame other = (Exame) obj;
        if (!Objects.equals(this.codigoExame, other.codigoExame)) {
            return false;
        }
        return true;
    }
    
    
}
