/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dto;

/**
 *
 * @author fabiomf
 */
public class Anamnese  implements BaseDTO{
 
    private String pergunta;
    
    private String resposta;

    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    @Override
    public String toString() {
        return "Anamnese{" + "pergunta=" + pergunta + ", resposta=" + resposta + '}';
    }
    
    
}
