/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dto;

/**
 *
 * @author fabiomf
 */
public class Medico extends Pessoa{
    
    private Integer crm;

    public Medico() {
        
    }
    
    public Medico(Integer crm) {
        this.crm = crm;
    }
    
    public Integer getCrm() {
        return crm;
    }

    public void setCrm(Integer crm) {
        this.crm = crm;
    }

    @Override
    public String toString() {
        return "Medico{" + "crm=" + crm + '}';
    }
    
    
    
}
