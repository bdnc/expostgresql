/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dto;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author fabiomf
 */
public class Paciente extends Pessoa {
    
    private Prontuario prontuario;

    private List<Consulta> listaConsultas = new ArrayList<>();
    
    public Prontuario getProntuario() {
        return prontuario;
    }

    public void setProntuario(Prontuario prontuario) {
        this.prontuario = prontuario;
    }

    public List<Consulta> getListaConsultas() {
        return listaConsultas;
    }

    public void setListaConsultas(List<Consulta> listaConsultas) {
        this.listaConsultas = listaConsultas;
    }
    
    
}
