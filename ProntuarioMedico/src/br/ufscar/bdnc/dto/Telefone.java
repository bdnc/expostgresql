/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.dto;

import java.util.Objects;

/**
 *
 * @author fabiomf
 */
public class Telefone implements BaseDTO {
    
    private Integer ddd;
    
    private Integer prefixo;
    
    private Integer numero;
    
    private Integer ramal;
    
    private TipoTelefoneEnum tipoTelefoneEnum;

    public Integer getDdd() {
        return ddd;
    }

    public void setDdd(Integer ddd) {
        this.ddd = ddd;
    }

    public Integer getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(Integer prefixo) {
        this.prefixo = prefixo;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getRamal() {
        return ramal;
    }

    public void setRamal(Integer ramal) {
        this.ramal = ramal;
    }

    public TipoTelefoneEnum getTipoTelefoneEnum() {
        return tipoTelefoneEnum;
    }

    public void setTipoTelefoneEnum(TipoTelefoneEnum tipoTelefoneEnum) {
        this.tipoTelefoneEnum = tipoTelefoneEnum;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.ddd);
        hash = 37 * hash + Objects.hashCode(this.tipoTelefoneEnum);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Telefone other = (Telefone) obj;
        if (!Objects.equals(this.ddd, other.ddd)) {
            return false;
        }
        if (this.tipoTelefoneEnum != other.tipoTelefoneEnum) {
            return false;
        }
        return true;
    }
    
    
}
