/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufscar.bdnc.util;

import javax.swing.JTextField;

/**
 *
 * @author fabio.medeiros
 */
public class ConverterUtil {
    
    public static Integer textFieldToInteger(JTextField  jTextField) {
        
        Integer integerValue = null;
        if (!jTextField.getText().trim().isEmpty()) {
            
            try {
                integerValue = Integer.parseInt(jTextField.getText());
            } catch (NumberFormatException e) {
                
                throw  new NumberFormatException("Erro na conversão, espera-se um valor numérico para: " + jTextField.getText());
            }
        }
        
        return integerValue;
    }
}
