/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fabiomedeirosfaria
 */
public class Conexao {
    
    private static final String USER = "postgres";
    
    private static final String PASSWORD = "123456";
    
    private static final String HOST = "localhost";
    
    private static final String DATABASE_NAME = "clinica";
    
    private static final String URL = "jdbc:postgresql://"+ HOST + ":5432/" + DATABASE_NAME;
    
    private static Conexao conexao;
    
    private Connection con;
    
    private Conexao() {
        
    }
    
    public static Conexao getInstance() {
        if (conexao == null) {
            conexao = new Conexao();
        }
        
        return conexao;
    }
    
    public Connection getConnection() {
        try {
            if (con != null && !con.isClosed()) {
                return con;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            Class.forName("org.postgresql.Driver");
            
            con = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch ( ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        
        return con;
    }
    
    public void closeConnection() {
        
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
