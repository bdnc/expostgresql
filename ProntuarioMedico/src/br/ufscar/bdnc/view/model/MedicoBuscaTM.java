/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.view.model;

import br.ufscar.bdnc.dto.Medico;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author fabiotakeda
 */
public class MedicoBuscaTM extends DefaultTableModel {
    private List<Medico> dados = new ArrayList<Medico>();
    public MedicoBuscaTM(List dados) {
        this.dados = dados;
    }


    @Override
     public  int getColumnCount() {
       return 3; //quantidade de campos do que terá na tabela
    }

     @Override
    public int getRowCount() {
       if (this.dados == null) {
          return 0;
       } else {
          return this.dados.size();
       }
    }

     @Override
    public Object getValueAt(int row, int column) {
       Medico medico = dados.get(row);   // RDV é o meu objeto bean com seus gets e sets


       Object retorno = null;
       if (column == 0) {   //pega os valores do objeto para retornar para a jTable
          retorno = (medico.getPrenome() + " " + medico.getSobrenome());
       } else if (column == 1) {
           retorno = medico.getCpf();
       } else if (column == 2) {
           retorno = medico.getCrm();
       }
       return retorno;

    }

     @Override
    public String getColumnName(int column) {
       String columnName = "";   //rotulo de cabeçalho das colunas do jTable
       if (column == 0) {
          columnName = "Nome";
       } else if (column == 1) {
           columnName = "CPF";
       } else if (column == 2) {
           columnName = "CRM";
       } 
      return columnName;
    }
}
