/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.view.model;

import br.ufscar.bdnc.dto.Pessoa;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Raquel
 */
public class PessoaBuscaTM extends DefaultTableModel {
    
    private List<Pessoa> dados = new ArrayList<Pessoa>();

    public PessoaBuscaTM(List dados) {
        this.dados = dados;
    }


    @Override
     public  int getColumnCount() {
       return 2; //quantidade de campos do que terá na tabela
    }

     @Override
    public int getRowCount() {
       if (this.dados == null) {
          return 0;
       } else {
          return this.dados.size();
       }
    }

     @Override
    public Object getValueAt(int row, int column) {
       Pessoa pessoa = dados.get(row);   // RDV é o meu objeto bean com seus gets e sets


       Object retorno = null;
       if (column == 0) {   //pega os valores do objeto para retornar para a jTable
          retorno = (pessoa.getPrenome() + " " + pessoa.getSobrenome());
       } else if (column == 1) {
           retorno = pessoa.getCpf();
       } 

       return retorno;

    }

     @Override
    public String getColumnName(int column) {
       String columnName = "";   //rotulo de cabeçalho das colunas do jTable
       if (column == 0) {
          columnName = "Nome";
       } else if (column == 1) {
           columnName = "CPF";
       } 
      return columnName;
    }
}
  