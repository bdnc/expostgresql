/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.view.model;

import br.ufscar.bdnc.dto.Telefone;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author fabiomedeirosfaria
 */
public class TelefoneTM extends DefaultTableModel{


    private List<Telefone> dados = new ArrayList<Telefone>();

    public TelefoneTM(List dados) {
        this.dados = dados;
    }


    @Override
     public  int getColumnCount() {
       return 5; //quantidade de campos do que terá na tabela
    }

     @Override
    public int getRowCount() {
       if (this.dados == null) {
          return 0;
       } else {
          return this.dados.size();
       }
    }

     @Override
    public Object getValueAt(int row, int column) {
       Telefone telefone = dados.get(row);   // RDV é o meu objeto bean com seus gets e sets


       Object retorno = null;
       if (column == 0) {   //pega os valores do objeto para retornar para a jTable
          retorno = telefone.getDdd();
       } else if (column == 1) {
           retorno = telefone.getPrefixo();
       } else if (column == 2) {
           retorno = telefone.getNumero();
       } else if (column == 3) {
           retorno = telefone.getRamal();
       } else if (column == 4) {
           retorno = telefone.getTipoTelefoneEnum();
       }

       return retorno;

    }

     @Override
    public String getColumnName(int column) {
       String columnName = "";   //rotulo de cabeçalho das colunas do jTable
       if (column == 0) {
          columnName = "DDD:";
       } else if (column == 1) {
           columnName = "Prefixo";
       } else if (column == 2) {
           columnName = "Número";
       } else if (column == 3) {
           columnName = "Ramal";
       } else if (column == 4) {
           columnName = "Tipo";
       }
       return columnName;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false; //super.isCellEditable(row, column);
    }


    
}
