/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufscar.bdnc.service.exception;

/**
 * Classe para exceções de lógica.
 * @author fabio.medeiros
 */
public class ServiceException extends Exception{
    
    public ServiceException(String message) {
        super(message);
    }
    
    public ServiceException(Throwable cause, String message) {
        super(message, cause);
    }
}
