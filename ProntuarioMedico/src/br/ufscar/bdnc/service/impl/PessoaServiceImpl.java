/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.service.impl;

import br.ufscar.bdnc.dao.MedicoDAO;
import br.ufscar.bdnc.dao.PacienteDAO;
import br.ufscar.bdnc.dao.PessoaDAO;
import br.ufscar.bdnc.dao.impl.MedicoDAOImpl;
import br.ufscar.bdnc.dao.impl.PacienteDAOImpl;
import br.ufscar.bdnc.dao.impl.PessoaDAOImpl;
import br.ufscar.bdnc.dto.Medico;
import br.ufscar.bdnc.dto.Paciente;
import br.ufscar.bdnc.dto.Pessoa;
import br.ufscar.bdnc.service.PessoaService;
import br.ufscar.bdnc.service.exception.ServiceException;
import java.util.List;

/**
 *
 * @author fabiomf
 */
public class PessoaServiceImpl implements PessoaService{

    private PacienteDAO pacienteDAO = new PacienteDAOImpl();
    
    private MedicoDAO medicoDAO = new MedicoDAOImpl();
    
    private PessoaDAO pessoaDAO = new PessoaDAOImpl();

    
    @Override
    public void incluirPessoa(Pessoa pessoa) throws ServiceException{
        
        this.validarPessoa(pessoa);
        
        if (pessoa instanceof Paciente) {
            Paciente paciente = (Paciente) pessoa;
                       
            pacienteDAO.inserir(paciente);
            
        } else if (pessoa instanceof Medico) {
            
            Medico medico = (Medico) pessoa;
            
            medicoDAO.inserir(medico);
        }
    }
    
    private void validarPessoa(Pessoa pessoa) throws ServiceException {
        
        if(pessoa.getCpf() == null) {
            throw  new ServiceException("CPF é requerido.");
        } else if (pessoa.getPrenome() == null || pessoa.getPrenome().isEmpty()) {
            throw  new ServiceException("Pré-nome é requerido.");
        } else if (pessoa.getSobrenome() == null || pessoa.getSobrenome().isEmpty()) {
            throw  new ServiceException("Sobrenome é requerido.");
        }
        
        Pessoa pessoaCPFExistente = pessoaDAO.consultarPessoaPorCpf(pessoa.getCpf());
        
        if (pessoaCPFExistente != null) {
            throw new ServiceException("Existe um cadastro com este CPF.");
        }
    }

    @Override
    public List<Paciente> consultarPacientePorNome(String nome) {

        return pacienteDAO.obterPacientePorNome(nome.trim());
    }

    @Override
    public List<Medico> consultarMedicoPorNome(String nome) {

        return medicoDAO.obterMedicosPorNome(nome.trim());

    }
        
}
