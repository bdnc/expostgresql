/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.service.impl;
import br.ufscar.bdnc.dao.ExameDAO;
import br.ufscar.bdnc.dao.impl.ExameDAOImpl;
import br.ufscar.bdnc.dto.Exame;
import br.ufscar.bdnc.service.ExameService;
import br.ufscar.bdnc.service.exception.ServiceException;
import java.util.List;

/**
 *
 * @author Raquel
 */
public class ExameServiceImpl implements ExameService{
    
    private ExameDAO exameDAO = new ExameDAOImpl();
    
    private void validarExame(Exame exame) throws ServiceException {
        
        if(exame.getDescricao() == null) {
            throw  new ServiceException("Descrição é requerido.");
        } else if (exame.getNome() == null) {
            throw  new ServiceException("Nome é requerido.");
        } else if (exame.getIndiceReferencia() == null) {
            throw  new ServiceException("Indice de Referência é requerido.");
        }

    }

    @Override
    public List<Exame> consultarExameNome(String nome) {
    return exameDAO.obterExamePorNome(nome.trim());
    }
}