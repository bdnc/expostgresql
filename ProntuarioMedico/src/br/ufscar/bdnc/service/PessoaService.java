/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.service;

import br.ufscar.bdnc.dto.Medico;
import br.ufscar.bdnc.dto.Paciente;
import br.ufscar.bdnc.dto.Pessoa;
import br.ufscar.bdnc.service.exception.ServiceException;
import java.util.List;

/**
 *
 * @author fabiomf
 */
public interface PessoaService {
    
    public void incluirPessoa(Pessoa pessoa) throws ServiceException;
    
    public List<Paciente> consultarPacientePorNome(String nome);
    
    public List<Medico> consultarMedicoPorNome(String nome);
}
