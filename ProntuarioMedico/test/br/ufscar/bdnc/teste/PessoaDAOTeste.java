/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufscar.bdnc.teste;

import br.ufscar.bdnc.dao.PessoaDAO;
import br.ufscar.bdnc.dao.impl.PessoaDAOImpl;
import br.ufscar.bdnc.dto.Pessoa;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author fabiomf
 */
public class PessoaDAOTeste {
    
    private PessoaDAO pessoaDAO;
    
    public PessoaDAOTeste() {
    }
    
    @Before
    public void setUp() {
        pessoaDAO = new PessoaDAOImpl();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void obterPessoaMedico() {
        
        List<Pessoa> listaPessoas = pessoaDAO.obterDados();
        
        System.out.println(listaPessoas);
    }
}
